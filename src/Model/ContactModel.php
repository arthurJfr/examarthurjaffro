<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;
/**
 *
 */
abstract class ContactModel extends AbstractModel
{
    protected static $table = 'contact';
    private $key;

    /**
     * @return $table
     */
    protected static function getTable()
    {
        return static::$table;
    }

    /**
     * @return array => all
     */


    public static function findById($id,$columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }

    public static function findByColumn($column,$value)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE ".$column." = ?",[$value],get_called_class(),true);
    }

    public static function count(){
        return App::getDatabase()->aggregation("SELECT COUNT(id) FROM " . self::getTable());
    }

    public static function delete($id,$columId = 'id')
    {
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }
    public static function insert($post){
        return App::getDatabase()->prepare("INSERT INTO ".self::getTable()."( sujet , email , message, created_at) VALUES (?,?,?,NOW())",[$post['sujet'],$post['email'],$post['message']],get_called_class(),true);
    }
    public static function all()
    {
        return App::getDatabase()->query("SELECT * FROM ".self::getTable(),get_called_class());
    }

    public function __get($key)
    {
        $method = 'get'.ucfirst($key);
        $this->key = $this->$method();
        return $this->key;
    }
}
