<?php

namespace App\Controller;

use App\Model\ContactModel;
use App\Service\Validation;
use App\Service\Form;
use Core\App;
use Core\Kernel\AbstractController;
use Core\Kernel\AbstractModel;

class ContactController extends AbstractController{
    public function index(){
        $errors = [];
        $v = new Validation();
        $form = new Form($errors);

      if (!empty($_POST['submitted'])){
          $post = $this->cleanXss($_POST);
          $errors['sujet'] = $v->textValid($post['sujet'],'sujet',3,150,);
          $errors['email'] = $v->emailValid($post['email']);
          $errors['message'] = $v->textValid($post['message'],'message',3,150,);
if ($v->IsValid($errors)){
    ContactModel::insert($post);
}
      }

        $this->render('app.contact.index',array(
            'form' => $form,
            'error' => $errors
        ),'base');
    }

    public function listingMess(){
$mess = ContactModel::all();

        $this->render('app.contact.listingMess',array(
'mess' => $mess
        ),'base');
    }
}